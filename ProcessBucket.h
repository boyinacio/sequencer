#ifndef _PROCESS_BUCKET_
#define _PROCESS_BUCKET_

using namespace std;

void processBucket(string* &segments, int &nSegments, bool forceMerge);

#endif